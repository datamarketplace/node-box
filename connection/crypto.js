const NodeRSA = require('node-rsa');

//https://www.npmjs.com/package/node-rsa

module.exports = {

    generateWinnerKey: function(callback){
        const key = new NodeRSA({b: 512});  //Generate a 256 bit key

        var privateKey = key.exportKey('private');

        var publicKey = key.exportKey('public');
        
        callback(privateKey, publicKey);
    },

    isPubKey: function(pubKey){
        const key = new NodeRSA();
        key.importKey(pubKey, 'public');
        return key.isPublic();
    }
}