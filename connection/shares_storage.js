var fs = require('fs');
var hash = require('object-hash');
const NodeCouchDb = require('node-couchdb');
//const truffle_connect = require('./connection/app.js');

//https://www.npmjs.com/package/express-fileupload
//https://www.npmjs.com/package/node-couchdb

//TODO Add feature to use external CouchDB devices

const couch = new NodeCouchDb();


const storeShare = function(auction_address, share){
    return new Promise(function(resolve, reject) {
        console.log(String(auction_address));
        let share_hash = hash([String(auction_address)]); //To retrive the key

        console.log("Document ID: " + share_hash)

        couch.insert("datamarketplace", {
            _id: share_hash,
            field: [auction_address, share]
        }).then(({data, headers, status}) => {
            console.log("Successful inserted");
            console.log(data);
            console.log(headers);
            console.log(status);
            resolve("Inserted");
        }, err => {
            // either request error occured
            // ...or err.code=EDOCCONFLICT if document with the same id already exists
            console.log(err);            
            if(err.code="Document insert conflict"){
                console.log("Document with same ID already exists");
                reject("ID already present");
            }else{
                console.log("Error" + err);
                reject(err);
            }
        }) 
    });
};


const getShare = function(auction_address){
    return new Promise( (resolve, reject) => {
        let share_hash = hash([String(auction_address)]); //To retrive the key
        console.log(String(auction_address));

        console.log("Document ID : " + share_hash)

        couch.get("datamarketplace", share_hash).then(({data, headers, status}) => {
            console.log(data);
            resolve(data);
        }, err => {
            console.log(err);
            if(err.code==="EDOCMISSING"){
                console.log("Document is missing");
                reject("Document is missing");
            }else if(err.code==="EUNKNOWN"){
                console.log("Unknow error");
                reject("Unknow error");
            }
        });
    });
}

const listDatabase = function(){
    return new Promise( function(resolve, reject){
        couch.listDatabases().then(dbs => {
            console.log(dbs);
            resolve(dbs);
        }, err => {
           console.log("There was an error while listing the databases");
           reject(err);
        });
    })
}


module.exports = {
    storeShare,
    listDatabase,
    getShare,
    isSharePresent: function(auction_address){ //Check if a Share is already present
        let share_hash = hash([auction_address]); //To retrive the key
        console.log(share_hash);
    }
}