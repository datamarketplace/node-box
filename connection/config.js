const NodeCouchDb = require('node-couchdb');


const couch = new NodeCouchDb();

const configDB = "config";

const getNodeID = function(){
    return new Promise(function(resolve, reject) {
        couch.get(configDB, "nodeID").then(({data, headers, status}) => {
            console.log(data);
            resolve(data);
        }, err => {
            console.log(err);
            if(err.code==="EDOCMISSING"){
                console.log("Configuration missing");
                reject("Configuration missing");
            }else if(err.code==="EUNKNOWN"){
                console.log("Unknow error");
                reject("Unknow error");
            }
        });
    });
}

const setNodeID = function(nodeID){
    return new Promise( function(resolve, reject) {
        couch.insert(configDB, {_id: "nodeID", field: [nodeID]}).then( ({data, headers, status}) => {
            console.log(data);
            resolve(true);
        }, err => {
            console.log(err);
            reject(err);
        });;
    });
}


module.exports = {
    getNodeID,
    setNodeID
}