const contract = require('truffle-contract');

const protocol_artifact = require('../build/contracts/Protocol.json');
const marketplace_artifact = require('../build/contracts/Marketplace.json');
var Protocol = contract(protocol_artifact);
var Marketplace = contract(marketplace_artifact);


module.exports = {
  /**
   * First function to be called that instantiate the contract 
   */
  start: function(callback) {
    var self = this;

    // Bootstrap the Protocol abstraction for Use.
    Protocol.setProvider(self.web3.currentProvider);
    Marketplace.setProvider(self.web3.currentProvider);

    // Get the initial account balance so it can be displayed
    self.web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        console.log("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        console.log("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }
      self.accounts = accs;
      self.account = self.accounts[2];

      callback(self.accounts);
    });
  },
  /**
   * Start the contract event listener which watches for contranct events
   * TODO: Add a check in order to verify if the contract is correctly instantiated
   */
  startEventListener: function(){
    var self = this;
    Protocol.setProvider(self.web3.currentProvider);
    Protocol.deployed().then(function(instance) {
      var event = instance.allEvents();
      console.log("Listening for events on auction");
      event.watch(function(error, result){
        if(!error)
          console.log("Contract event listener started");
        else
          console.log(error);
      });
    }).catch( (err) => {
      console.log(err);
    });    
  },

  startElectionListener: function(){
    var self = this;
    Marketplace.setProvider(self.web3.currentProvider);
    Marketplace.deployed().then(function(instance) {
      var event = instance.allEvents();
      console.log("Listening for election events");
      event.watch(function(error, result){
        if(!error)
          console.log("Marketplace event listener started: " + result);
        else
          console.log(error);
      });
    }).catch( (err) => {
      console.log(err);
    });  
  },
  refreshBalance: function(account, callback) {
    var self = this;

    // Bootstrap the Protocol abstraction for Use.
    Protocol.setProvider(self.web3.currentProvider);

    var meta;
    Protocol.deployed().then(function(instance) {
      meta = instance;
      return meta.getBalance.call(account, {from: account});
    }).then(function(value) {
        callback(value.valueOf());
    }).catch(function(e) {
        console.log(e);
        callback("Error 404");
    });
  },
  getAuctionInfo: function(callback) {
    var self = this;

    Protocol.setProvider(self.web3.currentProvider);

    var meta;
    Protocol.deployed().then(function(instance) {
      console.log("Contract Deployed");
      meta = instance;
      return meta.getAuctionInfo();
    }).then(function(values) {
      console.log("Values fetched");
      console.log(values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });

  },
  getWinnerPubKey: function(callback){
    var self = this;
    Protocol.setProvider(self.web3.currentProvider);
    Protocol.deployed().then(function(instance) {
      console.log("Contract Deployed");
      return instance.getWinnerPubKey();
    }).then(function(values) {
      console.log("Values fetched: " + values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });
  }
  ,
  setWinnerPubKey: function(pubkey, callback){
    var self = this;
    Protocol.setProvider(self.web3.currentProvider);
    Protocol.web3.eth.defaultAccount = Protocol.web3.eth.coinbase
    let defaultAccount = Protocol.web3.eth.defaultAccount
    let balance = Protocol.web3.eth.getBalance(Protocol.web3.eth.defaultAccount);
    Protocol.defaults({
      gasLimit: "1000000"
    });

    console.log("Balance: " + balance)

    Protocol.deployed().then(function(instance) {
      console.log("Contract Deployed");
      return instance.setWinnerPubKey.sendTransaction(pubkey, {from: defaultAccount, gas: 500000})  //TODO Improve this
    }).then(function() {
      console.log("Pubkey Setted");
      callback();
    }).catch(function(e) {
        console.log(e);
    });
  },
  getDealerPubKey: function(callback) {
    var self = this;
    Protocol.setProvider(self.web3.currentProvider);
    Protocol.deployed().then(function(instance) {
      console.log("Contract Deployed");
      return instance.getDealerPubKey();
    }).then(function(values) {
      console.log("Values fetched");
      console.log(values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });
  },
  setDealerPubKey: function(callback){
    var self = this;
    Protocol.setProvider(self.web3.currentProvider);
    Protocol.web3.eth.defaultAccount = Protocol.web3.eth.coinbase
    let defaultAccount = Protocol.web3.eth.defaultAccount
    let balance = Protocol.web3.eth.getBalance(Protocol.web3.eth.defaultAccount);
    Protocol.defaults({
      gasLimit: "1000000"
    });

    console.log("Balance: " + balance)

    Protocol.deployed().then(function(instance) {
      console.log("Contract Deployed");
      return instance.setDealerPubKey.sendTransaction(pubkey, {from: defaultAccount, gas: 500000})  //TODO Improve this
    }).then(function() {
      console.log("Pubkey Setted");
      callback();
    }).catch(function(e) {
        console.log(e);
    });
  },
  getActiveNodeNumber: function(callback){
    var self = this;
    Marketplace.setProvider(self.web3.currentProvider);
    Marketplace.deployed().then(function(instance) {
      return instance.getActiveNodeNumber();
    }).then(function(values) {
      console.log("Values fetched: " + values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });
  },
  sendCoin: function(amount, sender, receiver, callback) {
    var self = this;

    // Bootstrap the Protocol abstraction for Use.
    Protocol.setProvider(self.web3.currentProvider);

    var meta;
    Protocol.deployed().then(function(instance) {
      meta = instance;
      return meta.sendCoin(receiver, amount, {from: sender});
    }).then(function() {
      self.refreshBalance(sender, function (answer) {
        callback(answer);
      });
    }).catch(function(e) {
      console.log(e);
      callback("ERROR 404");
    });
  },
  getActiveAuctionNumber: function(callback){
    var self = this;
    Marketplace.setProvider(self.web3.currentProvider);
    Marketplace.deployed().then(function(instance) {
      return instance.getActiveAuctionNumber();
    }).then(function(values) {
      console.log("Values fetched: " + values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });    
  },
  getAuctionAddressFromID: function(auctionID, callback){
    var self = this;
    Marketplace.setProvider(self.web3.currentProvider);
    Marketplace.deployed().then(function(instance) {
      return instance.getAuctionAddressFromID(auctionID);
    }).then(function(values) {
      console.log("Values fetched: " + values);
      callback(values.valueOf());
    }).catch(function(e) {
        console.log(e);
    });    
  },
  getAuctionAddressFromID: function(auctionID){
    return new Promise(function(resolve, reject) {
      
    });
  }
}
