var Marketplace = artifacts.require("./Marketplace.sol");
var Protocol = artifacts.require("./Protocol.sol");

module.exports = function(deployer) {
  deployer.deploy(Marketplace);
  deployer.link(Marketplace, Protocol);  
  deployer.deploy(Protocol);
};
