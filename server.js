const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const Web3 = require('web3');
const truffle_connect = require('./connection/app.js');
const bodyParser = require('body-parser');
const crypto = require('./connection/crypto.js');
const sshare = require('./connection/shares_storage.js')
const cors = require('cors')
var base64 = require('base-64');
var jwt = require('jsonwebtoken');


const IPFS = require('ipfs')
const ipfsNode = new IPFS()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use('/', express.static('public_static'));

app.use(cors())
/**
 * Return current node couchdb databases
 */
app.get('/getDB', (req, res) => {
  console.log("**** GET /getDB ****");
  sshare.listDatabase().then( (output) => {
    console.log("Response:" + output);
    res.json(output);
  }).catch( (err) => {
    console.log("Error while listing couchDB databases: " + err)
    res.end(err);
  });
 
});


app.get('/status', (req, res) => {

  truffle_connect.getActiveNodeNumber( (result) => {
    let response = {activeNode: result};
    let json_answer = JSON.stringify(response);
    res.send(json_answer);
    res.status(200).end();
  });
});


app.get('/getAuctionInfo', (req, res) => {
  console.log("**** GET /getAuctionInfo *****");
  truffle_connect.getAuctionInfo((answer) => {
    console.log("*****Answer : " + answer);
    let json_answer = JSON.stringify(answer);
    res.send(json_answer);
  }); 
});

/**
 * NOTE:This function is here just for testing purpose, in a real world usage 
 * the winner sets its own public key on the client, not in the node
 * @param pubkey
 */
app.get('/setWinnerPubKey', (req, res) => {
  console.log("**** GET /setWinnerPubKey ****");

  //var auctionAddress = req.body.auction_address;
  var pubKey = req.body.pubkey;

  if(!crypto.isPubKey(pubKey))
    res.end("Invalid public key");

  truffle_connect.setWinnerPubKey(pubKey, function(){
    res.end(pubKey + " setted");
  });
});

/**
 * NOTE:This function is here just for testing purpose, in a real world usage 
 * the winner sets its own public key on the client, not in the node
 * @param none
 * @return generated pubkey
 */
app.get('/generatePubKey', (req, res) => {
  console.log("**** GET /generatePubKey ****");

  crypto.generateWinnerKey(function(privKey, pubKey) {
    console.log("Public Key:" + pubKey);
    console.log("Private Key: " + privKey);

    truffle_connect.setWinnerPubKey(pubKey, function(){
      res.end(pubKey + " setted");
    })
  })

});

app.post('/getPublicKey', (req, res) => {
  console.log('****** POST /getPublicKey')

  let owner = req.query.of; //dealer of winner

  if(owner === "winner"){
    truffle_connect.getWinnerPubKey( (pubkey) => {
      //pubkey = base64.decode(pubkey);
      console.log("PubKey Extracted: " + pubkey)
      res.json(pubkey);
    });
  }else if(owner === "dealer"){
    truffle_connect.getDealerPubKey( (pubkey) => {
      pubkey = base64.decode(pubkey);
      console.log("PubKey Extracted: " + pubkey)
      res.json(pubkey);
    });   
  }

});

/**
 * POST uploadShare
 * @param body.auction_address: the auction address
 * @param body.data: share's data
 * @param body.token dealer JWT token
 *  
 * @return "Inserted" on success, "ID present" if the same document already exists
 * 
 * TODO Change to PUT instead of POST
 * 
 */
app.post('/uploadShare/:auctionAddress', (req, res) => {
  console.log("**** POST /uploadShare ****");
  
  var auctionAddress = req.params.auctionAddress;
  var data = req.body.data;
  var token = req.body.token

  const sshare = require('./connection/shares_storage.js')

  console.log("Auction Address: " + auctionAddress + "\nData: " + data);

  truffle_connect.getDealerPubKey( (pubkey) => {
    console.log("Dealer PubKey retrived: " + pubkey);

    let decoded_pubkey = base64.decode(pubkey);

    jwt.verify(token, decoded_pubkey, { algorithm: 'RS256' }, (err, decoded) => {
      console.log("Error: " + err);
      console.log("Decoded :" + decoded);

      if(err !== null){
        res.send(err);
        res.status().end();
      }

      //If everything is Ok store the share

      sshare.storeShare(auctionAddress, data).then( (output) => {
        console.log("External :" + output);  
        res.send(output);
        res.status(200).end();
      })
      .catch(function(error) {
        console.log(error);
        res.send(error);
        res.status(400).end();
      });

    });  

  });
});


/**
 *  POST getShare
 *  @param body.auction_address   auction address related to share
 *  @param body.token             winner JWT token
 * 
 *  @returns base64 encoded share
 * 
 */

app.post('/getShare/:auctionAddress', (req, res) => {
  console.log("***** POST /getShare *****");

  let auctionAddress = req.params.auctionAddress;
  let token = req.body.token

  truffle_connect.getWinnerPubKey( (pubkey) => {

    console.log("Winner PubKey retrived: " + pubkey);

    jwt.verify(token, decoded_pubkey, { algorithm: 'RS256' }, (err, decoded) => {
      console.log("Error: " + err);
      console.log("Decoded :" + decoded);

      if(err !== null){
        res.send(err);
        res.status().end();
      }

      //If everything is Ok 

      sshare.getShare(auctionAddress).then( (result) =>{
        console.log("Data Fetched : " + result);
        res.json(result);
        res.status(200).end();
      }).catch( (err) => {
        console.log(err);
        res.send(err);
        res.status(400).end();
      });
    });
  })
});


/**
 * This function return a valid JWT token
 * @param priv_key
 */

app.post('/getJWT/:auctionAddress', (req, res) => {
  var jwt = require('jsonwebtoken');
  var config = require('./connection/config.js');

  console.log(config);

  var privkey = req.body.priv_key;
  var auctionAddress = req.params.auctionAddress;
  
  config.getNodeID().then( (response, err) => {
    let nodeID = res.field;
    //Once the nodeID is extracted, start signing
    
    var dec_privKey = base64.decode(privkey);

    jwt.sign({ auctionAddress: auctionAddress, generatedBy: nodeID}, dec_privKey, { algorithm: 'RS256' }, (err, token) => {
      console.log("Error: " + err);
      console.log("Token :" + token);
      if(err !== null){
        res.send(err);
        res.status(400).end();
      }
      res.send(token);
      res.status(200).end();
    });
  }).catch( (err) => {
    res.send(err);
    res.status(400).end();
  });
})


/**
 *  GET /checkJWT
 *  
 *  This function verify if the provided JWT token is valid.
 *  The public key necessary for the checking process is retrived from 
 *  the contract (on-chain).
 * 
 *  @param   POST  token
 *  @param   GET   owner
 * 
 *  @returns 200 - On success
 *  @returns 401 - On invalid token
 *  @returns 400 - On invalid command
 * 
 */

app.post('/checkJWT/:owner', (req, res) => {
  console.log("**** POST /checkJWT/" + req.params.owner + " ****");
  var jwt = require('jsonwebtoken');

  var token = req.body.token;
  var owner = req.params.owner;

  if(owner === "dealer"){
    console.log("JWT Dealer Request");
    truffle_connect.getDealerPubKey( (pubkey) => {  //First get the winner's pubKey
      console.log("Public Key Retrived: " + pubkey);
      pubkey = base64.decode(pubkey);

      jwt.verify(token, pubkey, { algorithm: 'RS256' }, (err, decoded) => {
        console.log("Error: " + err);
        console.log("Decoded :" + decoded);

        if(err !== null){
          res.send(err);
          res.status().end();
        }else{
          res.send(decoded);
          res.status(200).end();
        }
      });
    });
  }else if (owner === "winner"){
    console.log("JWT Winner Request");
    truffle_connect.getWinnerPubKey( (pubkey) => {  //First get the winner's pubKey
      if(pubkey === ""){
        res.send("Winner Public Key not published on the contract");
        res.status(400).end();
      }

      console.log("Public Key Retrived: " + pubkey);
      pubkey = base64.decode(pubkey);

      jwt.verify(token, pubkey, { algorithm: 'RS256' }, (err, decoded) => {
        console.log("Error: " + err);
        console.log("Decoded :" + decoded);

        if(err !== null){
          res.send(err);
          res.status(400).end();
        }else{
          res.send(decoded);
          res.status(200).end();
        }
      });
    });    
  }else{
    res.send("Token Type must be specified");
    res.status(400).end();
  }


});




app.get('/getAuctionList', (req, res) =>{
  console.log('**** GET /getAuctionList ****');

  truffle_connect.getActiveAuctionNumber((req, res) => {
    let auction_num = res;

    for(let i=0; i< auction_num; i++){
      //truffle_connect
    }

  });
});



app.get('/setNodeID/:nodeID', (req, res) => {
  console.log('**** GET /setNodeID ****');

  var config = require('./connection/config.js');

  var nodeID = req.params.nodeID;

  console.log("Node ID : " + nodeID)

  config.setNodeID(nodeID).then( (response, req) => {
    console.log("Node ID set up");

    res.status(200);
    res.end();
  }, err => {
    res.status(400).send(err);
    res.end();
  });

});


app.listen(port, () => {

  // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
  truffle_connect.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  truffle_connect.startEventListener();
  truffle_connect.startElectionListener();

  console.log("Express Listening at http://localhost:" + port);

});



ipfsNode.on('ready', () => {
  // Ready to use!
  // See https://github.com/ipfs/js-ipfs#core-api
})