# Node Box

This code is part of a project that aims to create a distributed and confidential marketplace for data. 
This source is related to nodes of the off-chain storage network.

**WARNING** : This code is untested and unrealiable and it should not be used in any production environment.

### Installation

This project uses Truffle Framwork (https://truffleframework.com/). 
After cloning the repository, you should execute the following steps

The software works on Ethereum blockchain, so you must start a local version with `ganache-cli`.

To compile contracts `truffle compile`.

To deploy contracts `truffle migrate`.

Once contracts is deployed you could start the node by typing `npm start`.


### Admin Interface
Once the node started a web interface is available at "localhost:3000"

### API Documentation
A dealied and updated version of the current APIs can be found at the following URL: https://app.swaggerhub.com/apis-docs/gr3yc4t/DatamarketplaceNode/1.0.0




