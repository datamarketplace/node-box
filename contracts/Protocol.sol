pragma solidity ^0.4.17;

contract Protocol {
	
    struct Participant{
        address addr;
        uint256 bid;    //The current bid 
        uint8 reputation;   //The Participant reputation (fetched from Datamarketplace contracts)
    }
    
    enum NodeType { share_keeper, perturbabion_keeper }
    
    struct Node{
        uint256 ID;                  //Node identifier
        address contract_address;   //Multiple Nodes can share a single contract
        uint8 reputation;
        string phy_address;
        bytes32 pub_key;                   
        NodeType node_type; //Sybil attack Prevention
    }
    
    Participant winner;
    Participant dealer;

    uint256[] participant_nodes;            //Array cointaining the ID of the participant nodes
    mapping (uint256 => Node) public nodes_list;   //Storage for nodes data

    address[] participant_users;
    mapping (address => Participant) public users_list;

    //---------- Auction Vars -------------

    uint highestBid;
    bool auction_ended;
    string auction_name;
    string file_description;
    address auctionCreator;

    uint16 constant max_description_len = 120;

    //-------------------------------------
    uint max_node_number;                   
    bool send_perturbation;
    address public winnerAddress;
    string public winner_pub_key;     //TODO Improve for better security (RSA 512 bit)
    string public dealer_pub_key;

    bytes32 validator_hash;     //hash(hash(a0+ pi)
    

    event auctionWin(address winner);
    event sendSharesToNodes();              //Directed to the dealers
    event publishPubKey(address winner);    //Directed to the winner  

    constructor() public{

        auctionCreator = msg.sender;
        dealer.addr = msg.sender;   //Choose just one of them
        auction_ended = false;
        auction_name = "Test Auction";
        file_description = "An example auction for testing the functionalities of the datamarketplace system";
        winner.addr = 0;
        winner_pub_key = "";
        highestBid = 0;
        //Generate custom data for testing
        Participant storage tmp;    //Temporary store for participant

        //Participant 1
        address addr1 = 0x6a2f01b939ca6c036c6bc275a59cbddaf38f2500;
        participant_users.push(addr1);
        tmp.addr = addr1;
        tmp.bid = 200;
        tmp.reputation = 20;
        users_list[addr1] = tmp;

        //Participant 2
        address addr2 = 0xcdf27866418452977b330dd8e05a765a6ed28b8f;
        participant_users.push(addr2);
        tmp.addr = addr2;
        tmp.bid = 250;
        tmp.reputation = 30;
        users_list[addr2] = tmp;

        //Participant 3
        address addr3 = 0xcdf27866418452977b330dd8e05a765a6ed28b8f;
        participant_users.push(addr3);
        tmp.addr = addr3;
        tmp.bid = 150;
        tmp.reputation = 50;
        users_list[addr3] = tmp;

        //Specify the dealer's address
        dealer.addr = 0xdda0607ea5ea78441aa5650f49f3b8bf6b5079a9;

        //Generate some nodes
        Node storage tmp_node;
        uint256 node_id = 1000;
        tmp_node.ID = node_id;
        participant_nodes.push(node_id);
        tmp_node.contract_address = 0xdda0607ea5ea78441aa5650f49f3b8bf6b5079a9;
        tmp_node.phy_address = "localhost:3000";
        tmp_node.node_type = NodeType.share_keeper;
        nodes_list[node_id] = tmp_node;

        node_id = 1001;
        tmp_node.ID = node_id;
        participant_nodes.push(node_id);
        tmp_node.contract_address = 0xdda0607ea5ea78441aa5650f49f3b8bf6b5079a9;
        tmp_node.phy_address = "localhost:3001";
        tmp_node.node_type = NodeType.share_keeper;
        nodes_list[node_id] = tmp_node;

    }

    /**
    *   
     */
    function getAuctionInfo() public view returns (address, address, uint, string, string){
        return (winner.addr, dealer.addr, participant_users.length, auction_name, file_description);
    }

    function getWinnerInfo() public view returns (address, uint256, uint8){
        if(auction_ended)
            return (winner.addr, winner.bid, winner.reputation);
        else    
            return (0, 0, 0);
    }


    function setWinnerPubKey(string _pubkey) public{
        require(winner.addr == msg.sender, "Only the dealer can set his own Public Key");
        winner_pub_key = _pubkey;
    }

    function getWinnerPubKey() public view returns (string){
        return (winner_pub_key);
    }

    function setDealerPubKey(string _pubkey) public{
        require(auctionCreator == msg.sender, "Only the dealer can set his own Public Key");
        dealer_pub_key = _pubkey;
    }

    function getDealerPubKey() public view returns (string){
        return dealer_pub_key;
    }

    function getNodesNumber() public view returns (uint){
        return (participant_nodes.length);
    }

    function getNodeInfo(uint256 number) public view returns (string, address, uint8){
        uint256 id = participant_nodes[number];
        Node tmp = nodes_list[id];
        return (tmp.phy_address, tmp.contract_address, tmp.reputation);
    }


    function declareWinner() public{
        //Classical Algorithm for finding the max in an array
        uint n = participant_users.length;
        uint max = 0;
        address winner_addr;
        for(uint i = 0; i<n; i++){
            address current_addr = participant_users[i];
            if(users_list[current_addr].bid > max){
                max = users_list[current_addr].bid;
                winner_addr = current_addr;
            }
        }

        winner = users_list[winner_addr];
        auction_ended = true;
        emit auctionWin(winner.addr);
    }

    function getHighestBid() public view returns(uint){
        return highestBid;
    }

    function bid(uint amount) public payable{
        require(amount > highestBid, "Bid must be greater than the highest");

        highestBid = msg.value;
        //Participant storage part = Participant({addr: msg.sender, bid: msg.value, reputation: 0});
        //users_list[msg.sender] = part;
    }

}
